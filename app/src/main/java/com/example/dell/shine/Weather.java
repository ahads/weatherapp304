package com.example.dell.shine;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.widget.TextView;

public class Weather extends AppCompatActivity {

    TextView txt_city, txt_details, txt_temp, txt_humidity,  txt_weather, txt_update;  //declares the textviews we are using in the mainactivity

    Typeface weatherFont;  //the weather font is the icon


    //the buttons on the actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);






        weatherFont = Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/weathericons-regular-webfont.ttf");  //get the font from this .ttf
        txt_city = (TextView)findViewById(R.id.txt_city);   //the declared variables find the 'id' of each button
        txt_update = (TextView)findViewById(R.id.txt_update);
        txt_details = (TextView)findViewById(R.id.txt_detail);
        txt_temp = (TextView)findViewById(R.id.txt_temp);
        txt_humidity = (TextView)findViewById(R.id.txt_humidity);
        txt_weather = (TextView)findViewById(R.id.txt_weather);
        txt_weather.setTypeface(weatherFont);


        //run this function in the background,
        Function.placeIdTask asyncTask =new Function.placeIdTask(new Function.AsyncResponse() {
            public void processFinish(String weather_city, String weather_description, String weather_temperature, String weather_humidity,  String weather_updatedOn, String weather_iconText, String sun_rise) {
                txt_city.setText(weather_city);
                txt_update.setText(weather_updatedOn);
                txt_details.setText(weather_description);
                txt_temp.setText(weather_temperature);
                txt_humidity.setText("Humidity: "+weather_humidity);
                txt_weather.setText(Html.fromHtml(weather_iconText));
            }
        });
        //execute the function
        asyncTask.execute("25.180000", "89.530000"); //  asyncTask.execute("Latitude", "Longitude")



    }


}
