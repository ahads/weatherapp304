package com.example.dell.shine;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class Registration extends AppCompatActivity {

    DatabaseHelper helper = new DatabaseHelper(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
    }
    public void onSignUpClick(View v){
        if (v.getId() == R.id.Bsignupbutton){
            Intent i = new Intent(this, SignIn.class);
            startActivity(i);
            EditText name = (EditText) findViewById(R.id.TFname);
            EditText email = (EditText) findViewById(R.id.TFemail);
            EditText uname = (EditText) findViewById(R.id.TFuname);
            EditText pass1 = (EditText) findViewById(R.id.TFpass1);
            EditText pass2 = (EditText) findViewById(R.id.TFpass2);


            String namestr = name.getText().toString();     //convert the given values to string values
            String emailstr = email.getText().toString();
            String unamestr = uname.getText().toString();
            String pass1str = pass1.getText().toString();
            String pass2str = pass2.getText().toString();



          if (!pass1str.equals(pass2str)){   //if first password and second "confirm password" dont match --> show toast message.
                Toast pass = Toast.makeText(Registration.this, "Password dont match" , Toast.LENGTH_SHORT);
                pass.show();
            }
            else{
              //insert in DB
              Contact c =  new Contact();      //if they do then make then retrieve the given string values and store them as something
              c.setName(namestr);
              c.setEmail(emailstr);
              c.setUname(unamestr);
              c.setPass(pass1str);

              helper.insertContact(c);

            }

        }
    }
}
