package com.example.dell.shine;
//classes imported
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SignIn extends AppCompatActivity {
    DatabaseHelper helper = new DatabaseHelper(this);  //use the helper from databasehelper


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signin);
    }
public void onButtonClick(View v){   //onbuttonclick function
    if (v.getId() == R.id.Blogin){  //specifies Button log in

        EditText btn = (EditText)findViewById(R.id.TFuname);
        String str = btn.getText().toString();   //converts to string
        EditText b = (EditText)findViewById(R.id.TFpassword);
        String pass = b.getText().toString();


        //if given password matches then do something
        String password = helper.searchPass(str);
        if (pass.equals(password))
        {
            Intent i = new Intent(this, Options.class);
          //  i.putExtra("user",str);//if above is correct then show the " _ class."
            startActivity(i);
        }
        else {    //else show toast message in quotes.
            Toast temp = Toast.makeText(SignIn.this, "Doesnt match existing credentials" , Toast.LENGTH_SHORT);
            temp.show();
        }
    }
    if (v.getId() == R.id.Bsignup){ //see above
        Intent i = new Intent(this, Registration.class);
        startActivity(i);
    }



}


}