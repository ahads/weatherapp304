package com.example.dell.shine;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Options extends AppCompatActivity {


    public static final int REQUEST_CAPTURE = 1;
    public static final int CAMERA_REQUEST = 10;
    public static final int CAMERA_PERMISSION_REQEUSTCODE = 5;
    ImageView result_photo;
    private ImageView imgSpecimenPhoto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        imgSpecimenPhoto = (ImageView) findViewById(R.id.imgSpecimenPhoto);


    }

    public void onPhotoClick (View v){
        if (checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
        invokeCamera();
    }
    else {
            String[] permissionRequest = {android.Manifest.permission.CAMERA};
            requestPermissions(permissionRequest, CAMERA_PERMISSION_REQEUSTCODE);

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION_REQEUSTCODE){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED){
                invokeCamera();
            }
            else{
                Toast.makeText(this, "Need permission granted to use camera", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void invokeCamera() {
        Intent camera_intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File pictureDirectory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        String pictureName = getPictureName();
        File imageFile = new File(pictureDirectory, pictureName);
        Uri pictureUri = Uri.fromFile(imageFile);
        camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, pictureUri );
        startActivityForResult(camera_intent, CAMERA_REQUEST);
    }

    private String getPictureName() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String timeStamp = sdf.format(new Date());
        return "PlantPlacesImage" +  timeStamp + ".jpg";
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
     if (resultCode == RESULT_OK){
         if (requestCode == CAMERA_REQUEST) {
            // Bitmap cameraImage = (Bitmap) data.getExtras().get("data");
            // imgSpecimenPhoto.setImageBitmap(cameraImage);

         }
     }
    }

    /*  Button click = (Button) findViewById(R.id.captureButton);
        //result_photo = (ImageView)findViewById(R.id.img);
        if (!hasCamera()){
            click.setEnabled(false);
        }
    }
    public boolean hasCamera()
    {
        return getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_ANY);
    }
    public void launchCamera(View v){
        Intent i = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(i, REQUEST_CAPTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAPTURE && resultCode == RESULT_OK){
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            result_photo.setImageBitmap(photo);
        }
    }*/



    public void onLogOut(View v) {
        if (v.getId() == R.id.logoutButton) { //see above
            Intent i = new Intent(this, SignIn.class);
            startActivity(i);
        }
    }

    public void onWeatherClick(View v) {
        if (v.getId() == R.id.weatherButton) { //see above
            Intent i = new Intent(this, Weather.class);
            startActivity(i);
        }
    }

    public void onCityClick(View v) {
        if (v.getId() == R.id.improvementButton) { //see above
            Intent i = new Intent(this, Weather.class);
            startActivity(i);
        }
    }
}








