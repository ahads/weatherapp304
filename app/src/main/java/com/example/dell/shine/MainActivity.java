package com.example.dell.shine;

        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.animation.Animation;
        import android.view.animation.AnimationUtils;
        import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainactivity);
        imageView = (ImageView) findViewById(R.id.loadingIcon);
        Animation anim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.animation_welcome);
        imageView.setAnimation(anim);


        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                finish();
                startActivity(new Intent(getApplicationContext(), SignIn.class));

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

    }
}
